## Trisquel builder config

This is a work in progress.

### Running the Builder on Your Machine

 * sudo apt-get install git live-build
 * git clone https://devel.trisquel.info/aklis/live-config.git
 * cd live-config && sudo lb build

### Documentation
Full documentation of `live-build` can be found at [https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html](https://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html)